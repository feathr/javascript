module.exports = {
  rules: {
    // enforces that all collections define a model attribute in their
    // definition
    'backbone/collection-model': 2,

    // warns when defaults are not defined at the top of a Model definition.
    'backbone/defaults-on-top': 1,

    // warns when event callback scope is not passed into `on`
    'backbone/event-scope': 1,

    // warns when one of `tagName`, `className` or `events` hash is not defined
    // at top of a View definition.
    'backbone/events-on-top': [
      1,
      [
        'tagName',
        'className',
        'id',
      ],
    ],

    // warns when initialize is not the top method definition in a
    // View/Model/Collection definition.
    'backbone/initialize-on-top': [
      1,
      {
        View: [
          'tagName',
          'className',
          'events',
          'id',
        ],
        Model: [
          'defaults',
        ],
        Collection: [
          'model',
        ],
      },
    ],

    // enforces that Models have a `defaults` property in their definition.
    'backbone/model-defaults': 2,

    // enforces that the Model.changed property is not manually modified in
    // client code.
    'backbone/no-changed-set': 2,

    // enforces that the Collection.models property is not accessed directly
    // in client code.
    'backbone/no-collection-models': 2,

    // warns when the default constructor is being overridden.
    'backbone/no-constructor': 1,

    // enforces that the View.el property is not manually assigned in client
    // code.
    'backbone/no-el-assign': 2,

    // enforces that the Model.attributes property is not manually accessed in
    // client code.
    'backbone/no-model-attributes': 2,

    // warns when the native jquery object is used inside a View.
    'backbone/no-native-jquery': [1, 'all'],

    // warns when `silent` option is passed to a function that would otherwise
    // trigger an event.
    'backbone/no-silent': 1,

    // enforces that the Collection.models property is not accessed directly
    // from within a View
    'backbone/no-view-collection-models': 2,

    // enforces that the Model.attributes property is not accessed directly
    // from within a View
    'backbone/no-view-model-attributes': 2,

    // enforces that the Events.on/off binding methods are not used to bind a
    // View to its Model's events.
    'backbone/no-view-onoff-binding': 2,

    // warns when using native jquery object to access DOM elements not within
    // the View's `el`.
    'backbone/no-view-qualified-jquery': 1,

    // enforces that any overrides to View.render function must return `this`.
    'backbone/render-return': 2,
  },
};
