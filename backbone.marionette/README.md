# Feathr Backbone/Marionette Style Guide

*A mostly reasonable approach to Backbone and Marionette*

Mostly taken from Joanne Daudier's Marionette Style Guide (https://gist.github.com/jdaudier/32d7373a817d477be479), @jmeas's Backbone event spec (https://github.com/jmeas/backbone.event-spec) and the documentation of Ilya Volodin's backbone plugin for eslint (https://github.com/ilyavolodin/eslint-plugin-backbone)

## Table of Contents

  1. [Application](#markdown-header-application)
  1. [Events](#markdown-header-events)
  1. [Controllers](#controllers)
  1. [Views](#views)
  1. [Regions](#regions)
  1. [Templates](#templates)
  1. [Models and Collections](#models-and-collections)
  1. [Modules](#modules)


## Application

- [1.1](#application--role) **Role**: The role of the `Mn.Application` object is to be a unified entry point for your application. This means that the application exposes the methods that start the application, but your app should not be made up of child properties on the Application object.

Communication between modules should happen through `Bb.Radio`, not through the Application object's properties.

Channels should be accessed by name through the `Bb.Radio` object, not by importing the module you want to talk to and grabbing it's channel.

> Why? This is the basis for Marionette's Evented architecture. Strictly enforcing event-based messaging between modules makes code more independent, reusable and robust (less brittle).

```javascript
// bad
import {App} from '../../../app.js';
const model = new App.Entities.Models.FooModel();

// also bad
import {Models} from '../../entities/models';
const model = new Models.FooModel();

// still bad
import {Models} from '../../entities/models';
const modelChannel = Models.modelChannel;
const model = modelChannel.request('fooModel');

// good
import {Radio} from radio;
const model = Radio.channel('foo').request('fooModel');
```

## Events

- [2.1](#events--scope) **Scope**: When you use `Events.on` to setup an event listener and callback, you should explicitly provide a scope for the callback.

> Why? This makes event handling more consistent and easier to reason about.

```javascript

// bad
Bb.Model.extend({
  initialize() {
      this.on('change', this.onChange);
  }
});

// good
Bb.Model.extend({
  initialize() {
      this.on('change', this.onChange, this);
  }
});

```

- [2.2](#events--placement) **Placement**: If an event hash is defined on a View, it should be the defined at the top of the View definition, excepting properties that affect the identity of View in the DOM (`id`, `tagName`, `className`).

> Why? It improves readability of your View logic.

```javascript

// bad
Mn.View.extend({
  initialize() {
      //...
  },

  events: {
    //..
  },
});

// good
Mn.View.extend({
  events: {
    //...
  },

  initialize() {
      //...
  },
});

// also good
Mn.View.extend({
  id: 'TheBlock',
  tagName: 'div',
  className: 'Block',
  events: {
    //...
  },

  initialize() {
      //...
  },
});

```

- [2.3](#events--silent) **Silent**: Do not pass the `silent` option into a method that would otherwise trigger an event. Prefer handling whether the should be acted on in the event listener instead.

> Why? Silencing events is convenient, but can cause unexpected behavior in other parts of the application. Handle special cases where the cases are relevant and leave the general case unchanged.

```javascript

// bad
Bb.Model.extend({
  intialize() {
      this.set('test', 'test', {silent:true});
  },
});

// good
Bb.Model.extend({
  intialize() {
      this.set('test', 'test');
  },
});

// ...

Mn.View.extend({
  events: {
    change: 'onChange'
  },

  onChange(model) {
    if (model.hasChanged('test')) {
      return;
    } else {
      // ...
    }
  }
});

```

- [2.4](#events--binding) **Binding**: Do not use `Events.on` to bind a View to its Model's events. Prefer `Events.listenTo`. `Events.on` should only be used to bind an object's event listener to the object's own events.

> Why? `Events.on` is not memory-safe. The view will not be properly garbage collected if the `Events.on` binding is not explicitly destroyed with `Events.off` before the View is destroyed. Bindings created with `Events.listenTo` are automatically destroyed when the View is destroyed.

```javascript

// bad
Mn.View.extend({
    initalize() {
        this.model.on("change", this.render);
    }
});

// good
Mn.View.extend({
    initialize: function() {
        this.listenTo(this.model, "change", this.render);
    }
});

// also good
Mn.View.extend({
    initialize: function() {
        this.on("show:thing", this.onShowThing);
    }
});

```

- [2.5](#events--naming) **Naming**: Event names should follow the format `adverb:verb:noun`.

The separator `:` is used to separate the three pieces of the event name. Compound pieces in each event name should be written in camelCase, i.e `show:myView` instead of `show:my:view` or `show:my_view`.

The adverb piece is optional and frequently unnecessary. The most common use is the `before` adverb which is used in Backbone and Marionette to create events that are triggered before particular events take place, such as `before:render`. Note that is not acceptable to use `after` for events that are triggered after something takes place - that is represented by the event verb on it's own. So use `render` instead of `after:render`, for instance.

The verb piece should always be in the present tense. The verb is the only required part of an event name. So use `render` instead of `rendered`. The verb should be, grammatically a verb, which corresponds to the process or action that just took place.

The noun is the last part of the event name, and is optional like the adverb. An event may have at most a single noun. It should be a descriptive word that corresponds to the object that the verb is happening to. The noun should be omitted if the object that the verb is happening to is the same as the object listening to the event. So, if you want a model to listen to it's own changes, you should write it as `change` instead of `change:model`.

- [2.5.1](#events--naming-listeners) **Naming Listeners**: Event listener methods should be named according the event name that they are listening to.

For example, a method that listens to an event named `before:show:child` should be named `onBeforeShowChild`.

1. Capitalize first letter. `Before:show:child`
1. Prepend `on` to the event name: `onBefore:show:child`
1. Capitalize letters that immediately follow colons. `onBefore:Show:Child`
1. Remove colons. `onBeforeShowChild`

## Controllers

- [3.1](#controllers--separation-of-concerns) **Separation of Concerns**: Controllers respond to routes, create new view instances, prepare view dependencies (models & collections), and listen and respond to view events in the case that the event is intended for something outside of the nested view structure the event originated from.

Controllers provide the implementation/entry point logic for routes defined in the App's `Mn.AppRouter`s, and should be attached as the `controller` property for a corresponding `Mn.AppRouter`.

Use `Mn.Object` to create controllers. This gives them the `Events` mixin functionality out of the box.

## Views

- [4.1](#views--initialize) **Initialize**: A view's `initialize` method should be the first method listed in the view's definition.

```javascript
// bad
Mn.View.extend({
  onRender() {
    // ...
  },

  initialize() {
    // ...
  },
});

// good
Mn.View.extend({
  initialize() {

  },

  onRender() {
    // ...
  },
});

// also good
Mn.View.extend({
  // events is not a method so it can appear before initialize
  events: {
    // ...
  },

  initialize() {

  },

  onRender() {
    // ...
  },
});
```

- [4.2](#views--constructor) **Constructor**: Do not override the `constructor` method of a View. Prefer overriding the `initialize` method to attach behavior to a view to execute upon instantiation of the object.

```javascript
// bad
Mn.View.extend({
  constructor() {
    // ...
  }
});

// good
Mn.View.extend({
  initialize() {

  }
});
```

- [4.3](#views--el) **`el` and `$el`**: Do not assign values to the `Mn.View.el` or `Mn.View.$el` properties directly. Use `Mn.View.setElement` instead. Or better yet, let the `Region` do its job and attach the view to the appropriate element in the DOM on its own.

> Why? Views automatically bind to DOM events via the `delegateEvents` method - this method is never called if you attach the `el` directly.

```javascript
// bad
Mn.View.extend({
  initialize() {
    this.$el = $('.test');
  },
});

// ok
Mn.View.extend({
  initialize() {
    this.setElement($('.test'));
  },
});

// good
Mn.View.extend({
  initialize() {
    // ...
  },

  onAttach() {
    // attached to the DOM because the View was shown via a call to `Region.show`.
  },
});
```

- [4.4](#views--collection-models) **View.collection.models**: Do not access the `models` property of a View's `collection` directly. If you need to manipulate the models in a `View.collection`, use the collection's methods, such as `get`, `at`, `add`, `remove`, etc.

> Why? Manipulations done directly on the `models` property of the collection do not produce events and so are not captured by the events functionality of Backbone and Marionette.

```javascript
// bad
Mn.View.extend({
  onRender() {
    this.collection.models.pop();
  },
});

//good
Mn.View.extend({
  onRender() {
    this.collection.pop();
  },
});
```

- [4.5](#views--model-attributes) **View.model.attributes**: Do not access the `attributes` property of a View's model directly from within the View. Prefer the model's `get` and `set` methods instead.

> Why? Manipulations done directly on the `attributes` property of the model do not produce events and so are not captured by the events functionality of Backbone and Marionette.

```javascript
// bad
Mn.View.extend({
  onRender() {
    this.model.attributes.foo = "bad";
  },
});

//good
Mn.View.extend({
  onRender() {
    this.model.set({ foo: "good" });
  },
});
```

- [4.6](#views--qualified-jquery) **Qualified jQuery**: Do not use the native jQuery object (an imported `$`) to access DOM elements in the View's `$el`. Prefer the View's `$` or `$el` properties.

> Why? Views should not do anything directly on DOM elements outside the View's root element. Using the native jQuery object to access DOM elements inside the View's root element is not performant because it is not scoped to the View's root element.

```javascript
// bad
Mn.View.extend({
  onRender() {
    $('.some-selector', this.$el);
  },
});

//good
Mn.View.extend({
  onRender() {
    this.$('.some-selector');
  },
});
```

- [4.7](#views--render) **Render**: If you must override a View's render function, make sure it returns `this`. This should not be necessary though, because Marionette provides a useful implementation of `render` for you which already returns `this`. Prefer not to override `Mn.View.render`.

```javascript
// bad
Mn.View.extend({
  render() {
    // ...
  },
});

//good
Mn.View.extend({
  render() {
    // ...
    return this;
  },
});
```

- [4.8](#views--separation-of-concerns) **Separation of Concerns**: Views are responsible for visual presentation and responding to events that originate in the DOM. Views do not instantiate **their own** models or collections, instead being given preloaded and instantiated models or collections from a controller or parent view. This implies that views are allowed to instantiate or fetch models or collections with the purpose of passing those entities into a child view. The only case where this is not true is if the model being instantiated is a direct conceptual child of the view's model. These models should be instantiated by the view's model and simply passed to the child view by the parent view.

- [4.9](#views--ui-hash) **ui Hash**: Views that have ui elements that need to be manipulated or accessed must define a `ui` hash, such that jQuery selectors can be cached and reused. Avoid using jQuery to find elements altogether if possible.

`ui` hash elements should be named with a `$` prepended to the name to make it clear that they are jQuery objects.

`ui` hash element names should be camelCase with the form `$[adjective[Adjective]...]Noun`. Zero or more adjectives prepended to a noun. The noun should describe what category of ui element it is. For instance: `$submitButton`, `$dateInput`, or `$textarea`. Adjectives should not be used unless necessitated by the presence of multiple ui elements that would be described by the same category of ui element. So if you have a modal with a single button to acknowledge the message, the button should be called `$button` instead of `$okButton`.

```javascript
Mn.View.extend({
  ui: {
    $submitButton: '[data-js-ui-button-submit]'
  },

  onRender() {
    this.ui.$submitButton.text('Click here!');
  },
});
```

- [4.10](#views--toJSON) **toJSON**: As per the Backbone and Marionette docs, do not use model.toJSON or collection.toJSON when rendering views. This allows you to override toJSON without affecting rendering.

`toJSON` is a model method used to serialize data to send back to the server. Views shouldn’t be using it. Say your server sends and must be sent dates as a string, but on the client, you want to use Moment.js., and you want to filter out the Moment before you send it to the server. Use `toJSON` to filter out Moment. Your view might want the Moment, though, so you just write a `toJSON` equivalent view method in your views. This way you won't find yourself in a pickle if your view and server need different data.

- [4.11](#views--regions) **Regions**: Regions should be named according to the role that they serve within a view, rather than their position or content. The name should follow the form `[adjective[Adjective]...]Noun`. Zero or more adjectives prepended to a noun. The noun should describe what component the region is supplying to the view. For instance: `partnerList`, `nameInput`, or `breadcrumbs`. Adjectives should not be used unless necessitated by the presence of multiple regions that would be described by the same base component. So if you have a form with a single input inside it, the region that holds the name input view would be named simply `input` instead of `nameInput`.

Regions should be accessed via the `Mn.View.getRegion` method rather than directly on the View or Application object. Marionette v3 actually removed the region properties from direct assignment on the View and Application objects so it's no longer possible to access Regions from anywhere except the `getRegion` function. This is to prevent naming collisions on Application and View properties.

Regions should be added to a view when some part of the view can be broken down into a distinct subsystem of interaction. For instance, a form might be a view, but each input in the form would be a distinct view inside a region of the form, because each input functions individually with rich, responsive interaction in its own right, as well as a part of the whole form.

## Models and Collections

- [5.1](#models-and-collections--defaults) **Defaults**: When creating a Backbone model, you should always specify defaults. While this is not required by Backbone itself, it increases readability of the code and helps clearly identify properties that should be expected to be in the model. If the model is served from the server-side as JSON, it's sometimes very hard to understand which properties can be expected to be available.

```javascript
// bad
Bb.Model.extend({
  initialize() {
    // ...
  }
});

// good
Bb.Model.extend({
  initialize() {
    // ...
  },

  defaults: {
    // ...
  },
});
```

- [5.2](#models-and-collections--changed) **Changed**: The `Model.changed` property is automatically computed by Backbone and modified when any property of the model has been updated. Manually changing it can lead to problems.

```javascript
// bad
Mn.View.extend({
  onClickButton() {
    this.model.changed = false;
  },
});

// good
Mn.View.extend({
  onClickButton() {
    if (this.model.changed) {
        // ...
    }
  },
});
```

- [5.3](#models-and-collections--collection-models) **Collection.models**: Instead of accessing models collection directly from within collections, use `get()`, `at()` or underscore functions. If you are looking for length of the collection, use `this.length` instead. If you want to modify collection use `this.add`, `this.remove`, `this.push` and `this.pop` methods instead.

```javascript
// bad
Bb.Collection.extend({
  initialize() {
     _.first(this.models);
  },
});

// good
Bb.Collection.extend({
  initialize() {
    this.at(0);
  },
});
```

- [5.4](#models-and-collections--model-attributes) **Model.attributes**: Instead of accessing attributes object directly from within models, use `get()` or `set()` functions. Backbone setters do more then just assign value, they also keep track of when model has been last modified. If you work with attributes object directly, `changed` and `changedAttributes` will not be updated and events will not be captured.

```javascript
// bad
Bb.Model.extend({
  initialize() {
    _.first(this.attributes);
  },
});

// good
Bb.Model.extend({
  initialize() {
      this.set('test', true);
  },
});
```

## Modules

- [6.1](#modules--import) **Importing**: Modules should be imported using the es6/es2015 import syntax. Marionette should be imported as `Mn` and Backbone should be imported as `Bb`.

```javascript
import Bb from 'backbone';
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
```

- [6.2](#modules--structure) **Structure**: The application should be broken down into `activities` and `services`.

Activities are groups of 'pages' in the app, attached to a pillar of the app's information architecture. Each activity is represented by a folder that contains the routers, controllers, views, models, styles, and templates necessary for that module of the application. This keeps related components conceptually near each other for easy access within each modular component, while still making each modular component accessible to the rest of the application via `Backbone.Radio` channels.

Services are similar but instead of exposing routes, services expose channels which can be requested against from anywhere in the app trigger behaviors that are independent of the page being viewed - such as showing a modal, showing an alert/flash, global header/navbar behavior, etc. Services should be configurable be useful for the activity they are being requested from. They are otherwise similarly structured as activities, defining within their module folder all of the controllers, views, models, styles, and templates necessary for the service to work and display appropriately.

Common view behavior between modules should be handled by `Mn.Behavior` classes which can be imported in as needed.

**Example**
```
  -app
    -activities
      -events
        -list
          views.js
          styles.scss
          itemTemplate.hbs
          layoutTemplate.hbs
        -detail
          views.js
          styles.scss
          template.hbs
        -new
          views.js
          styles.scss
          template.hbs
        router.js
        models.js
        collections.js
      -people
        (same as events)
      -campaigns
        (same as events)
    -services
      -modals
        -alert
          views.js
          template.hbs
        -confirm
          views.js
          template.hbs
        views.js
        template.hbs
        styles.scss
        channel.js
    -behaviors
      form.js
    index.js
    app.js
  index.html
```
